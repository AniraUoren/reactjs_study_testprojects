const initialState = {
    login: "",
    pass: "",
    isAuthorised: false
};

export const reduser = (state = initialState, action) => {
    switch (action.type) {
        case "CHANGE_LOGIN":
            return {
                ...state,
                login: action.payload
            };
        case "CHANGE_PASS":
            return {
                ...state,
                pass: action.payload
            };
        case "SUCCESS_AUTHORIZATION":
            return {
                ...state,
                isAuthorised: true
            };
        case "FALSE_AUTHORIZATION":
            return {
                ...state,
                isAuthorised: false
            };
        default:
            return state;
    }

};
