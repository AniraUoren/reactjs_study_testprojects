export const changeLoginInput = (text) => {
    return {
        type: "CHANGE_LOGIN",
        payload: text
    }
};

export const changePassInput = (text) => {
    return {
        type: "CHANGE_PASS",
        payload: text
    }
};

export const submitAuthorization = (login, pass, isAuthorised) => {
    if (login == "User" && pass == "123" && isAuthorised == false){
        return {
            type: "SUCCESS_AUTHORIZATION"
        }
    } else {
        return {
            type: "FALSE_AUTHORIZATION"
        }
    }
};
