import React from "react";
import { connect } from "react-redux";
import * as actions from "../actions";
import "../css/css.css"

const InputComp = ({login, pass, isAuthorised, changeLoginInput, changePassInput, submitAuthorization}) => {

    const handlerChangingLogin = (event) => {
        changeLoginInput(event.target.value);
    };

    const handlerChangingPass = (event) => {
        changePassInput(event.target.value);
    };

    const handleSubmitForm = (event) => {
        event.preventDefault();
        submitAuthorization(login, pass, isAuthorised);
    };

    return (
        <div>
            <form action="#" className= "formClass" onSubmit={handleSubmitForm}>
                <input type="text" onChange={handlerChangingLogin} placeholder="Please, enter login"/>
                <input type="password" onChange={handlerChangingPass} placeholder="Please, enter password"/>
                {isAuthorised ? <button type="submit">Logout</button> : <button type="submit">Login</button>}
            </form>
        </div>

    );

};

/*Отвечает за то, чтобы была возможность прокинуть из хранилища значения. State это значения в хранилище*/
const mapStateToProps = (state) => {
    return {
        login: state.login,
        pass: state.pass,
        isAuthorised: state.isAuthorised
    };
};

export default connect(mapStateToProps, actions)(InputComp);
